var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');


// compile Sass files
gulp.task('sass', function() {
  return gulp.src('style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(''))
    .pipe(browserSync.reload({
        stream: true
    }))
});


// minify css
gulp.task('minify-css', ['sass'], function() {
  return gulp.src('css/*.css')
    .pipe(cleanCss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(''))
    .pipe(browserSync.reload({
        stream: true
    }))
});



// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})


// reload browser on any file change
gulp.task('watch', ['browserSync',/* 'sass', 'minify-css'*/], function() {
  // gulp.watch('*.scss', ['sass']);
  // Reloads the browser whenever HTML or JS files change
  gulp.watch('*.html', browserSync.reload);
  gulp.watch('*.css', browserSync.reload);
  gulp.watch('*.js', browserSync.reload);
});


// Default task (runs at initiation: gulp --verbose)
gulp.task('default', ['watch']);
