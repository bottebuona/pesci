jQuery(document).ready(function($) {

  "use strict";

  var sea = $(".sea");
  var seaWidth = sea.width();
  var seaHeight = sea.height();
  var minCrossDuration = 12, maxCrossDuration = 18;
  var destination, nextDestination, rotation, fishWidth, fishHeight, xVal, yVal, timeFactor;

  /* make fish swim */
  $(".sea-fish svg").each( function(i) {

    var fish = $(this);
    fishWidth = fish.width();
    fishHeight = fish.height();

    // random position fish for the first time
    xVal = random(0 , seaWidth - fishWidth );
    yVal = random(0, seaHeight - fishHeight );

    if(  randomChoice(true, false) ) {
      rotation = 0;
      destination = -fishWidth;
      nextDestination = seaWidth;
      timeFactor = (xVal / seaWidth);
    } else {
      rotation = 180;
      nextDestination = -fishWidth;
      destination = seaWidth;
      timeFactor = 1 - (xVal / seaWidth);
    }

    TweenLite.set( fish, {
      x: xVal,
      y: yVal,
      rotationY: rotation
    });

    upAndDown(this, seaHeight - fishHeight );

    backAndForth(this, destination, nextDestination, random(minCrossDuration, maxCrossDuration) * timeFactor );

  });
  /* animate fin of fish inside svg sprite. */
  moveLateralFin( $('.lateral-fin'));
  moveBackFin( $('.backfin') );


  function moveLateralFin(fin) {
    var tl = new TimelineLite({ delay: 1, onComplete: moveLateralFin, onCompleteParams: [fin]})
    tl.to(fin, 0.5, { rotationX: 40, transformOrigin: "right bottom" });
    tl.to(fin, 0.5, { rotationX: 0, delay:0.5, transformOrigin: "right bottom" });
  }

  function moveBackFin(fin) {
      var tl = new TimelineLite( { delay: 1, onComplete: moveBackFin, onCompleteParams: [fin] } );
      tl.to(fin, 0.5, { rotationY: 35, transformOrigin: "left 50%" } );
//      tl.to(fin, 0.5, { rotationY: 0, transformOrigin: "left 50%" } );
      tl.to(fin, 0.5, { rotationY: -35, transformOrigin: "left 50%" } );
      tl.to(fin, 0.5, { rotationY: 0, transformOrigin: "left 50%" } );

  };

  /*
  * Let fish move horizontally */
  function backAndForth(fish, destination, nextDestination, duration) {
    console.log(duration);
    var tl = new TimelineLite({onComplete: backAndForth, onCompleteParams: [fish, nextDestination, destination, random( minCrossDuration, maxCrossDuration)]}); // new route for coming back
    tl.to(fish, duration, { x: destination, ease:Linear.easeNone });
    tl.set(fish, {rotationY: "+=" +180, delay: 2 });

  };


  /*
  * Let fish move vertically */
  function upAndDown(fish, maxDeep) {
    TweenLite.to(fish, random(10, 15), {y: random(0, maxDeep), delay: 1, onComplete: upAndDown, onCompleteParams: [fish, maxDeep]});

  }


  function random(min, max) {
    if (max == null) {
      max = min;min = 0;
    }
    return Math.random() * (max - min) + min;
  }


  function randomChoice(choice1, choice2) {
    return Math.random() < 0.5 ? choice1 : choice2;
  }
});
